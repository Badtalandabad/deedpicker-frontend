import Vue from 'vue'
import App from './App.vue'
import store from './store'
import apiPlugin from './plugins/apiPlugin'
import localStoragePlugin from "./plugins/localStoragePlugin"

Vue.config.productionTip = false
Vue.use(apiPlugin)
Vue.use(localStoragePlugin)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
