
/**
 * Helper to work with data structures, like days, activities
 */
class DataStructureHelper {
  /**
   * Initializes Days collection
   *
   * @param days JS object with timestamp as keys and day objects as values
   *
   * @return {Object}
   */
  initDays(days) {
    for (let day of Object.values(days)) {
      day.date = new Date(day.date);
      if (isNaN(day.date.getTime())) {
        throw new Error(`Invalid data in Days collection ${day}`);
      }
      day.date.setHours(0, 0, 0, 0);
      if (day.activityStatuses === undefined) {
        day.activityStatuses = {};
      }
    }
    return days;
  }

  /**
   * Returns new Day object
   *
   * @param date             Date
   * @param activityStatuses JS object with activity ids as keys and boolean statuses as values
   *
   * @return Object
   */
  makeNewDayObject(date, activityStatuses = {}) {
    let newDate;
    if (date instanceof Date) {
      newDate = new Date(date.getTime());
    } else {
      newDate = new Date(date);
    }
    if (isNaN(newDate.getTime())) {
      throw new Error('Invalid date argument in makeNewDayObject');
    }

    return {date, activityStatuses};
  }

  validateActivityName(activityName) {
    if (typeof activityName !== 'string') {
      return false;
    }
    return activityName.match(/^[a-z0-9]+[a-z0-9 ]*$/i);
  }
}

export {DataStructureHelper};
export default new DataStructureHelper();
