import Vue from 'vue';

const vue = new Vue();

const actions = {
  /**
   * Initializes data in the app (days and activities).
   * Retrieves them from API and saves them in the store
   *
   * @param context Vuex context
   * @param payload Payload {dateStart, dateEnd}
   */
  initData(context, payload) {
    vue.$apiGetAllActivities().then(response => {
      if (!response || !response.result) {
        throw new Error('Empty response from get-days');
      }
      context.commit('setActivities', {activities: response.result});

      //TODO: check it exists
      const mainActivity = context.getters.notSpecifiedActivity;
      const mainActivityId = mainActivity._id;
      context.commit('setMainActivityId', {activityId: mainActivityId});

      vue.$apiGetDaysInRange(payload.dateStart, payload.dateEnd, context.getters.mainActivityId)
        .then(response => {
          if (!response || !response.result) {
            throw new Error('Empty response from get-days');
          }
          context.commit('setDays', {days: response.result});
        });
    });
  },

  /**
   * Loads activities from API
   *
   * @param context Vuex context
   */
  loadActivities(context) {
    vue.$apiGetAllActivities().then(response => {
      context.commit('setActivities', {activities: response.result});
    });
  },

  /**
   * Loads days from API
   *
   * @param context Vuex context
   * @param payload Payload {dateStart, dateEnd, activityId}
   */
  loadDays(context, payload) {
    vue.$apiGetDaysInRange(payload.dateStart, payload.dateEnd, payload.activityId).then(response => {
      context.commit('setDays', {days: response.result});
    });
  }
};

export default actions;