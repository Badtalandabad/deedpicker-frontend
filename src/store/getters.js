const getters = {
  /**
   * Returns all activities
   *
   * @param state
   *
   * @return {[Object]}
   */
  allActivities: (state) => {
    return state.activities;
  },

  /**
   * Returns activity, doesn't matter which one
   *
   * @param state Vuex state
   *
   * @return {Object}
   */
  notSpecifiedActivity: (state) => {
    return Object.values(state.activities)[0];
  },

  /**
   * Returns text id of main activity
   *
   * @param state
   *
   * @return string
   */
  mainActivityId: (state) => {
    return state.mainActivityId;
  },

  /**
   * Returns activity with given id or undefined if none found
   *
   * @param activityId Id of the activity
   *
   * @return Object
   */
  activityById: (state) => (activityId) =>{
    return state.activities[activityId];
  },

  /**
   * Returns activity with given name or undefined if none found
   *
   * @param activityName Activity name
   *
   * @return Object
   */
  activityByName: (state) => (activityName) =>{
    return Object.values(state.activities).find(activity => activity.name === activityName);
  },

  /**
   * Returns day object with a certain date
   *
   * @param dateToFind  Date to filter
   *
   * @return Object
   */
  day: (state) => (dateToFind) => {
    if (!(dateToFind instanceof Date)) {
      throw new Error('Compared date should be an instance of Date')
    }
    const normalizedComparedDate = new Date(dateToFind.getTime());
    normalizedComparedDate.setHours(0, 0, 0, 0);
    return state.days[normalizedComparedDate.getTime()];
  },

  /**
   * Returns object with start and end dates of the chosen period
   *
   * @param state Vuex state object
   *
   * @return Object
   */
  period: (state) => {
    return {
      dateStart: new Date(state.period.dateStart.getTime()),
      dateEnd: new Date(state.period.dateEnd.getTime()),
    };
  },
};

export default getters;