import Vue from "vue";
import dataStructureHelper from "../library/dataStructureHelper";

const mutations = {
  /**
   * Saves array of activity objects in the store
   *
   * @param state      Vuex state
   * @param activities JS object with all activities
   */
  setActivities(state, {activities}) {
    state.activities = activities;
  },

  /**
   * Saves edited activity in the store
   *
   * @param state        Vuex state
   * @param activityId   Activity id
   * @param activityName Activity name
   */
  editActivity(state, {activityId, activityName}) {
    if(!dataStructureHelper.validateActivityName(activityName)) {
      throw new Error('Invalid activity name, use letters and digits.');
    }
    state.activities[activityId].name = activityName;
  },

  /**
   * Saves new activity in the store
   *
   * @param state        Vuex state
   * @param activityId   Activity id
   * @param activityName Activity name
   */
  addActivity(state, {activityId, activityName}) {
    const activity = {_id: activityId, name: activityName};
    Vue.set(state.activities, activity._id, activity);
  },

  /**
   * Deletes activity from the store
   *
   * @param state      Vuex state object
   * @param activityId Activity id
   */
  deleteActivity(state, {activityId}) {
    delete state.activities[activityId];
  },

  /**
   * Sets main activity in the store
   *
   * @param state      Vuex state
   * @param activityId Activity text id
   */
  setMainActivityId(state, {activityId}) {
    state.mainActivityId = activityId;
  },

  /**
   * Sets date period
   *
   * @param state     Vuex state
   * @param dateStart Date start object
   * @param dateEnd   Date end object
   */
  setPeriod(state, {dateStart, dateEnd}) {
    const dateStartToSet = new Date(dateStart.getTime());
    dateStartToSet.setHours(0, 0, 0, 0);

    const dateEndToSet = new Date(dateEnd.getTime());
    dateEndToSet.setHours(0, 0, 0, 0);
    state.period = {dateStartToSet, dateEndToSet};
  },

  /**
   * Saves hash map of day objects in the store
   *
   * @param state Vuex state
   * @param days  JS object with dates as keys and days as values
   */
  setDays(state, {days}) {
    state.days = dataStructureHelper.initDays(days);
  },

  /**
   * Switches day status for certain activity
   *
   * @param state      Vuex state
   * @param date       Date object to find day
   * @param activityId Activity text id
   */
  switchDayStatus(state, {date, activityId}) {
    const day = state.days[date.getTime()];
    if (!day) {
      const newDay = dataStructureHelper.makeNewDayObject(date, {[activityId]: true});
      Vue.set(state.days, date.getTime(), newDay);
    } else {
      day.activityStatuses[activityId] = !day.activityStatuses[activityId];
    }
  },

};

export default mutations;