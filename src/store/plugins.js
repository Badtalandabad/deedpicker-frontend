import Vue from 'vue'

const vue = new Vue();

const saveInStorageOnMutationPlugin = store => {
  store.subscribe((mutation, state) => {
    const payload = mutation.payload;

    switch (mutation.type) {
      case 'setActivities':
        vue.$localStorageSetActivities(payload.activities);
        break;
      case 'setDays':
        vue.$localStorageSetDays(payload.days);
        break;
      case 'switchDayStatus':
        vue.$localStorageSetDays(state.days);
        break;
      default:
    }
  });
};

export default [saveInStorageOnMutationPlugin];