import Vue from 'vue'
import Vuex from 'vuex'
import localStoragePlugin from '../plugins/localStoragePlugin'

import mutations from "./mutations";
import getters from "./getters";
import actions from "./actions";
import plugins from "./plugins";

Vue.use(Vuex);
Vue.use(localStoragePlugin);

const vueStore = new Vue();

const state = {
  activities: vueStore.$localStorageGetActivities(),
  mainActivityId: null,
  days: vueStore.$localStorageGetDays(),
  period: vueStore.$localStorageGetPeriod(),
};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  plugins,
})