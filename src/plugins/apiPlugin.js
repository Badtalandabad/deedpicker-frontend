const Api = {
  'BASE_URL': 'https://deedpicker.local/api/v1/',

  'ACTION_GET_ALL_ACTIVITIES': 'get-all-activities',
  'ACTION_EDIT_ACTIVITY': 'edit-activity',
  'ACTION_ADD_ACTIVITY': 'add-activity',
  'ACTION_DELETE_ACTIVITY': 'delete-activity',

  'ACTION_GET_DAYS_IN_RANGE':  'get-days-in-range',
  'ACTION_CHECKOUT_DAY':       'checkout-day',
};

Api.install = function (Vue, options) {
  Vue.prototype.$apiDateToText = (date) => {
    return date.getFullYear()
      + '-' + ((date.getMonth() + 1) + '').padStart(2, '0')
      + '-' + (date.getDate() + '').padStart(2, '0');
  };

  /**
   * Makes http request to API
   *
   * @param actionName     Name of the API action
   * @param data           Data to send. Optional
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$callApi = (actionName, data = {}) => {
    const endpoint = this.BASE_URL + actionName;

    const params = {
      method: 'POST',
      mode: 'cors',
      headers: {'Content-Type': 'application/json',},
      body: JSON.stringify(data),
    };

    return fetch(endpoint, params).then((response) => {
      if (!response.ok) {
        throw new Error('API responded with an error');
      }

      return response.json();
    }).catch(error => console.log(error));
    //TODO: add network failure processing
  };

  /**
   * Retrieves list of all activities
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$apiGetAllActivities = () => {
    return Vue.prototype.$callApi(Api.ACTION_GET_ALL_ACTIVITIES);
  };

  /**
   * Retrieves list of all activities
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$apiGetDaysInRange = (dateStart, dateEnd, activityId) => {
    const textDateStart = Vue.prototype.$apiDateToText(dateStart);
    const textDateEnd = Vue.prototype.$apiDateToText(dateEnd);
    return Vue.prototype.$callApi(Api.ACTION_GET_DAYS_IN_RANGE, {
      dateStart: textDateStart,
      dateEnd: textDateEnd,
      activityId,
    });
  };

  /**
   * Checks out a certain day
   *
   * @param date           Date instance
   * @param activityId     Activity text id
   * @param activityStatus Activity status (true/false)
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$apiCheckoutDay = (date, activityId, activityStatus) => {
    const textDate = Vue.prototype.$apiDateToText(date);
    return Vue.prototype.$callApi(Api.ACTION_CHECKOUT_DAY, {
      date: textDate,
      activityId,
      activityStatus,
    });
  };

  /**
   * API edit activity call
   *
   * @param activityName Activity name
   * @param activityId   Activity id
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$apiEditActivity = (activityName, activityId) => {
    return Vue.prototype.$callApi(Api.ACTION_EDIT_ACTIVITY, {activityId, activityName,});
  };

  /**
   * API add activity call
   *
   * @param activityName Activity name
   *
   * @return {Promise<Object>}
   */
  Vue.prototype.$apiAddActivity = (activityName) => {
    return Vue.prototype.$callApi(Api.ACTION_ADD_ACTIVITY, {activityName,});
  };

  Vue.prototype.$apiDeleteActivity = (activityId) => {
    return Vue.prototype.$callApi(Api.ACTION_DELETE_ACTIVITY, {activityId});
  };
};

export default Api;