const LocalStorage = {
  KEY_ACTIVITIES: 'activities',
  KEY_DAYS: 'days',
  KEY_PERIOD: 'period',
};

LocalStorage.install = function (Vue, options) {
  /**
   * Retrieves item from local storage
   *
   * @param key          Key in the local storage
   * @param defaultValue Value in case no such key found
   */
  Vue.prototype.$localStorageGetItem = (key, defaultValue) => {
    const json = window.localStorage.getItem(key);
    if (json) {
      return JSON.parse(json);
    }
    return defaultValue;
  };

  /**
   * Saves item in local storage
   *
   * @param key   Key in the storage
   * @param value Value to save
   */
  Vue.prototype.$localStorageSetItem = (key, value) => {
    window.localStorage.setItem(key, JSON.stringify(value));
  };

  /**
   * Retrieves activities from local storage
   *
   * @return Object
   */
  Vue.prototype.$localStorageGetActivities = () => {
    return Vue.prototype.$localStorageGetItem(LocalStorage.KEY_ACTIVITIES, {});
  };

  /**
   * Saves activities in local storage
   *
   * @param activities JS object with activity ids as keys and activity objects as values
   */
  Vue.prototype.$localStorageSetActivities = (activities) => {
    Vue.prototype.$localStorageSetItem(LocalStorage.KEY_ACTIVITIES, activities);
  };

  /**
   * Retrieves days from local storage
   *
   * @return Object
   */
  Vue.prototype.$localStorageGetDays = (defaultValue) => {
    return Vue.prototype.$localStorageGetItem(LocalStorage.KEY_DAYS, {});
  };

  /**
   * Saves days in local storage
   *
   * @param days JS object with stringified date objects as keys and day object as values
   */
  Vue.prototype.$localStorageSetDays = (days) => {
    Vue.prototype.$localStorageSetItem(LocalStorage.KEY_DAYS, days);
  };

  /**
   * Retrieves period from local storage
   * If no date is saved in storage, gets period from last month to next month
   *
   * @return Object
   */
  Vue.prototype.$localStorageGetPeriod = () => {
    const period = Vue.prototype.$localStorageGetItem(LocalStorage.KEY_PERIOD, {});

    if (!period.dateStart || !period.dateEnd) {
      const dateStart = new Date();
      dateStart.setDate(1);
      dateStart.setMonth(dateStart.getMonth() - 1);
      dateStart.setHours(0, 0, 0, 0);
      period.dateStart = dateStart;

      const dateEnd = new Date();
      dateEnd.setMonth(dateEnd.getMonth() + 2);
      dateEnd.setDate(0);
      dateEnd.setHours(0, 0, 0, 0);
      period.dateEnd = dateEnd;
    }

    return period;
  };

  /**
   * Saves period in local storage
   *
   * @param dateStart Start date
   * @param dateEnd   End date
   */
  Vue.prototype.$localStorageSetPeriod = (dateStart, dateEnd) => {
    if (!dateStart || !dateEnd) {
      throw new Error('dateStart and dateEnd arguments are mandatory');
    }
    if (dateStart > dateEnd) {
      throw new Error('dateStart should not be greater than dateEnd');
    }

    Vue.prototype.$localStorageSetItem(LocalStorage.KEY_PERIOD, {dateStart, dateEnd});
  };

};

export default LocalStorage;